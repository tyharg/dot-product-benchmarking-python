import concurrent.futures
import time
import random
import numpy as np


#Settings
#N = length of vectors
#vector_pairs = number of dot products to take
N = 500000
vector_pairs = 5

#dot_product(list, list):
#   takes two lists (vectors) as arguments
#   returns dot product using iterators
def dot_product(h, k):
    #print("numpy result: ", np.dot(h,k))
    return sum(vec[0] * vec[1] for vec in zip(h, k)) #return dot product


#main():
#   initialize data structure
#   fills list with random ints(0-1000000), from list[0] to list[N] 
#   load concurrent.futures.ProcessPoolExecutor()
#       map method and data structure to executor
#       launch executors
#       print out dot products
#       print time
def main():

    print("Initializing data (this may take a while).")
    #init data
    data1 = []
    data2 = []
    for i in range(vector_pairs):
        data1.append([])
        for z in range(len(data1)):
            for m in range(N):
                data1[i].append(random.randint(0,1000000))
    for i in range(vector_pairs):
        data2.append([])
        for z in range(len(data2)):
            for m in range(N):
                data2[i].append(random.randint(0,1000000))
                

    #load executor
    with concurrent.futures.ProcessPoolExecutor() as executor:
        start_time = time.time()
        for dot in zip(executor.map(dot_product, data1, data2)):
            print(dot)  #print dot product
    
        print("--- %s seconds ---" % (time.time() - start_time))



### Init Main ###
if __name__ == '__main__':
    main()
 
 
 
 
### Results ###
#
#        N          Pairs           Time
#       500           2             0.102
#      5000000        2             24.610
#      5000           10            0.215
#     500000          10            11.672
#     50000           1000          118.674
#
#

 
 
 
 
 
 
 
 
 